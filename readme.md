Superbird
---

superagent + bluebird = superbird :: Promisified superanget for the greater good.

Why
---

There's a plenty of ways to integrate superagent and bluebird, this is just one of them. It has a better API, by making `.end()` return a promise.

Usage
---
Install it

```bash
npm install --save superbird

```

Just like superagent, but promesified:

```javascript
var request = require('superbird')

request
  .post('/api/pet')
  .send({ name: 'Manny', species: 'cat' })
  .set('X-API-Key', 'foobar')
  .set('Accept', 'application/json')
  .end()
  .then((res) => {
  	// do what you want with my res.body
  })

```

License
---

MIT
